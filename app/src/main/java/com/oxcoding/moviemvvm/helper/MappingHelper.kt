package helper

import android.database.Cursor
import com.example.tubes.database.DatabaseContract
import com.example.tubes.entity.Feedback


object MappingHelper {
    fun mapCursorToArrayList(notesCursor: Cursor?): ArrayList<Feedback> {
        val notesList = ArrayList<Feedback>()
        notesCursor?.apply {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(DatabaseContract.NoteColumns._ID))
                val title = getString(getColumnIndexOrThrow(DatabaseContract.NoteColumns.TITLE))
                val description = getString(getColumnIndexOrThrow(DatabaseContract.NoteColumns.DESCRIPTION))
                val date = getString(getColumnIndexOrThrow(DatabaseContract.NoteColumns.DATE))
                notesList.add(Feedback(id, title, description, date))
            }
        }
        return notesList
    }
}